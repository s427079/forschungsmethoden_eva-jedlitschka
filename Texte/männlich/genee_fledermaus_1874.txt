[0001]

[0002]

[0003]

[0004]

[0005]

[0006]

[[1]/0007]
Arien und Geſänge
aus:
Die Fledermaus.
Komiſche Operette in 3 Acten
nach
Meîlhac und Halevy,
bearbeitet
von
C. Haffner und Richard Genée.
Muſik von Johann Strauß.
☞ Ueberſetzungsrecht vorbehalten. ☜


Das Bühnen-Aufführungsrecht kann nur ertheilt
werden durch die Theater- und Concert-Agentur
Guſtav Lewy
kaiſ.
[Abbildung]
kön.
Hof-Muſikalienhändler,
in Wien, IV. Schleifmühlgaſſe Ur. 6.


Verlag von G. Lewy. — Druck von Ch. Reißer & M. Werthner.

[[2]/0008]
Perſonen.


Gabriel von Eiſenſtein, Rentier.
Roſalinde, ſeine Frau.
Frank, Gefängniß-Director.
Prinz Orlofsky.
Alfred, ſein Geſanglehrer.
Doctor Falke, Notar.
Doctor Blind, Advocat.
Adele, Stubenmädchen Roſalindens.
Froſch, Gefängniß-Aufſeher.
Tänzerinnen der Oper.
	Melanie,
	Jda,
	Felicita,
	Sidi,
	Minni,
	Hermine,
	Sabine,
	Xandi,
	Bertha,
	Lori,
Ali-Bey, ein vornehmer Aegypter.
Ramuſin, japaneſiſcher Geſandtſchafts-Attaché.
Murray, ein reicher Amerikaner.
Cariconi, ein ſpaniſcher Spieler.
Jwan, Kammerdiener des Prinzen.
Herren und Damen. Diener.



[[3]/0009]
Erſter Act.


Nr. 1. Jntroduction.

Alfred (hinter der Scene).
Täubchen, das entflattert iſt,
Stille mein Verlangen,
Täubchen, das ich oft geküßt,
Laß Dich wieder fangen!
Täubchen, holdes Täubchen mein,
Komm’, o komm’ geſchwinde.
:,: Sehnſuchtsvoll gedenk ich Dein,
Holde Roſalinde! :,:


Adele.
Da ſchreibt meine Schweſter Jda —
Die iſt nämlich beim Ballet —
Wir ſind heut’ auf einer Villa,
Wo es hergeht flott und nett;
„Prinz Orlofsky,
„Der reiche Suitier,
„Gibt dort heute Abend
„Ein grand Souper,
„Kannſt Du eine Toilette
„Von der Gnäd’gen annexiren
„Und elegant Dich präſentiren,
„So will ich gerne dort ein Dich führen;
„Mach Dich frei nur und ich wette,
„Daß wir gut uns amüſiren,
„Lange Weile gibt es nie da!‟ —
So ſchreibt meine Schweſter Jda! —
Ach, ich glaub’s, ich zweifle nicht,
Wär gar zu gern von der Partie,
Aber ſchwierig iſt die G’ſchicht! —
Könnt’ ich nur fort — wüßt’ ich nur wie!


1*
[4/0010]

Alfred (von außen).
Täubchen, das entflattert iſt,
Stille mein Verlangen! u. ſ. w.


Adele.
Ach, ich darf nicht hin zu Dir!
Und Du ſehnſt Dich ſo nach mir,
Deiner heißgeliebten Nichte,
Gar zu traurig iſt die G’ſchichte,
Ach, warum ſchuf die Natur
Mich zur Kammerjungfer nur.


Roſalinde.
Nein, Du darfſt nicht hin zu ihr,
Wenn ſie ſich auch ſehnt nach Dir.
Wohl traurig klingt die G’ſchichte
Von der geliebten Nichte,
Ja, warum ſchuf die Natur
Dich zur Kammerjungfer nur.

Nr. 2. Terzett.

Roſalinde. Eiſenſtein. Blind.
Eiſenſtein.
Nein, mit ſolchen Advocaten
Jſt verkauft man und verrathen,
Da verliert man die Geduld!


Roſalinde. Nur Geduld!


Blind. Nur Geduld!


Eiſenſtein.
Statt, daß jetzt die Sach’ beendet,
Hat’s noch ſchlimmer ſich gewendet,
Und daran iſt Der nur ſchuld.


Blind.
Wer iſt ſchuld?


Roſalinde.
Der iſt ſchuld? Der wäre ſchuld?


Eiſenſtein.
Ja, der iſt ganz allein nur Schuld!


Roſalinde.
Der Herr Notar?


[5/0011]

Blind.
Das iſt nicht wahr!


Eiſenſtein.
Du wirſt’s ſchon ſeh’n!


Roſalinde.
Was iſt geſcheh’n? Erkläre Dich!


Eiſenſtein. So höre!


Blind.
Nein’ erſt will ich vertheid’gen mich!


Eiſenſtein.
Erſparen Sie ſich dieſe Müh’,
So etwas iſt nicht zu vertheid’gen.


Blind.
Mir ſcheint, Sie wollen mich beleid’gen!


Roſalinde.
Nur ruhig Blut,
Warum die Wuth?


Eiſenſtein.
Der Herr Notar ſchwatzte wie ein Narr!


Blind.
Herr Eiſenſtein fing an zu ſchrei’n!


Eiſenſtein.
Sie ſtottern ja bei jedem Wort.


Blind.
Sie ſchimpfen ja in einemfort!


Eiſenſtein.
Sie krähen wie ein Hahn!


Blind.
Sie ſind ein Grobian.


Eiſenſtein.
Sie ſind ein Dumm’rian!


Blind.
Sie ſind ſehr inhuman!


Beide.
Sie reden lauter Leberthran,
Und dreh’n ſich wie ein Wetterhahn.


Blind.
Sie raſen wie im Fieberwahn.
Und poltern wie ein Puterhahn.


[6/0012]

Roſalinde.
Doch ſchone Dein Organ,
Es ſei nun abgethan.
(Zu Blind.)
Das beſte wär’ — Sie geh’n hinaus —
Sonſt wird noch ein Scandal daraus!


Eiſenſtein. Ja, ſie hat Recht.


Blind. Nein, dieſer Ton!


Eiſenſtein.
Geh’n Sie hinaus!
Sonſt wird noch ein Scandal daraus.


Blind.
Hält man nicht aus,
Jch geh’ hinaus.


Roſalinde.
:,: Das Beſte wär’
Sie geh’n hinaus! :,:


Eiſenſtein.
Ja geh’ Sie, da iſt die Thür, hinaus, hinaus!


Blind.
Jch gehe ſchon, ja, ja, ich geh’ aus dieſem Haus!


Roſalinde.
Beruh’ge endlich dieſe Wuth,
Verurtheilt biſt Du, nun denn gut!
Ergib Dich d’rein
Und nach fünf Tagen,
Schon nach fünf Tagen,
Jſt die Geſchichte abgemacht!


Eiſenſtein.
Fünf Tage ſagſt Du?
Jetzt ſind’s gar acht!
Man hat mir drei noch zugeſchlagen. —
So weit hat’s dieſer Menſch gebracht,
Noch heute ſoll ich ſtellen mich,
Und komm’ ich nicht, ſo holt man mich!


Roſalinde.
Das iſt zu ſtark, das muß ich ſagen!


Eiſenſtein.
Nicht wahr?


[7/0013]

Roſalinde.
Ach, mein armer, armer Mann,
Noch heute alſo mußt Du d’ran?
Was ſoll ich Dir zum Troſte ſagen?
Wie ſoll ich das ertragen?


Eiſenſtein.
Ach, mit ſolchen Advocaten
Jſt verkauft man und verrathen!
Da verliert man die Geduld!


Roſalinde.
Und daran iſt Der nur ſchuld!


Blind.
Wer iſt ſchuld?


Roſalinde.
Sie ſind ſchuld!


Eiſenſtein.
Der iſt ganz allein nur ſchuld!


Blind.
Wenn Sie nur erſt wieder frei,
Proceſſiren wir auf’s Neu,
Und ich werd’ Jhnen dann ſchon zeigen,
Was ich kann!
Recurriren — Appelliren,
Reclamiren — Revidiren,
Recipiren — Subvertiren,
Devolviren — Jnvolviren,
Proteſtiren — Liquidiren,
Exerciren — Extorquiren,
Arbitriren — Reſumiren!


Eiſenſtein.
Hören Sie auf, ’s iſt genug!


Blind.
Exculpiren — Jnculpiren!


Roſalinde.
Hör’n Sie auf, ’s iſt genug!


Blind.
Calculiren — Concipiren,
Und Sie müſſen triumphiren!


[8/0014]

Roſalinde.
Ob Sie Berge von Papieren,
Auch dabei zuſammenſchmieren,
Sie werden ſchließlich ſich blamiren,
Ja, ja, ach ja, ach ja, Sie blamiren ſich!
Ach mit ſolchen Advocaten u. ſ. w.


Eiſenſtein.
Wenn Sie jetzt nicht retiriren,
Muß ich Sie herausbugſiren,
Und vielleicht noch ſchließlich maulſchelliren,
Muß ich Sie hinausbugſiren, ja bugſiren. —
Nein, mit ſolchen Advocaten u. ſ. w.


Blind.
Recurriren — Appelliren u. ſ. w.
Ja, Sie werden triumphiren ſicherlich!
Ach, wir armen Advocaten
Sollen immer helfen, rathen! ꝛc.

Nr. 3. Duett.

Falke.
Komm’ mit mir zum Souper,
Es iſt ganz in der Näh’!
Eh’ Du in der ſtillen Kammer
Laborirſt am Katzenjammer,
Mußt Du Dich des Lebens freu’n,
Ein fideler Bruder ſein!
Ballerinnen, leicht beſchwingt, in den blendendſten
Toiletten,
Feſſeln Dich mit Roſenketten,
Wenn die Polka lockend klingt!
Freundchen, glaub’ mir, das verjüngt, das verjüngt!
Bei rauſchenden Tönen
Jm blendenden Saal,
Mit holden Sirenen
Beim Göttermahl,
Da flieh’n die Stunden in Luſt und Scherz,
Da wirſt geſunden von allem Schmerz.


[9/0015]

Soll Dir Dein Gefängniß nicht ſchädlich ſein,
Mußt Du etwas thun, Dich zu zerſtreuen!
:,: Siehſt Du das ein? :,:


Eiſenſtein.
:,: Das ſeh’ ich ein! :,:
Doch meine Frau, die darf’s nicht wiſſen.


Falke.
Du wirſt zum Abſchied zärtlich ſie küſſen.
Sagſt gute Nacht, mein ſüßes Kätzchen.


Eiſenſtein.
Nein, nein, mein Mauſerl, ſage ich.
Mein ſüßes Mauſerl!
Denn als Katze ſchleich’ ich ſelbſt aus dem Hauſe mich!


Falke.
Süßes Mauſerl, ſüßes Mauſerl,
Sagſt Du zärtlich dann!
Und während ſie ſchläft ganz feſt,
Gehſt Du ſtatt in Deinen Arreſt
Mit mir zum himmliſchen Feſt.


Beide.
Mit mir
Dir
zum himmliſchen Feſt.
Jch führ’ Dich ein als Fremder;
Marquis Renard ſollſt Du dort ſein!
So wird man nichts erfahren können;
Willſt Du?


Eiſenſtein.
Ach, ich wär’ ſchon erbötig.


Falke.
Du mußt.


Eiſenſtein.
Wenn nur —


Falke.
Du mußt Dir’s vergönnen,
Zur Geſundheit iſt’s ja nöthig.


Eiſenſtein.
Ja, ich glaub’, Du haſt Recht,
Die Ausred’ iſt nicht ſchlecht!


[10/0016]

Falke.
Soll Dir das Gefängniß nicht ſchädlich ſein —


Eiſenſtein.
Soll mir das Gefängniß nicht ſchädlich ſein —


Beide.
Muß ich
Mußt Du
etwas thun,
Mich
Dich
zu zerſtreu’n!


Falke.
So kommſt Du?


Eiſenſtein.
Wer kann widerſteh’n?
Ja, ich bin dabei!


Falke.
Zum Teufel mit Deiner Leimſiederei!


Eiſenſtein.
Ein Souper uns heute winkt,
Wie noch gar kein’s dageweſen.
Schöne Mädchen auserleſen,
Zwanglos man dort lacht und ſingt:
Lalala!


Falke.
Ein Souper uns heute winkt,
Wie noch gar kein’s dageweſen.


Beide.
Hübſche Mädchen, auserleſen,
Zwanglos man dort lacht und ſingt:
Lalala!

Nr. 4. Terzett.

Roſalinde. Adele. Eiſenſtein.
Roſalinde.
So muß allein ich bleiben
Acht Tage ohne Dich?
Wie ſoll ich Dir beſchreiben
Mein Leid, ſo fürchterlich?
Wie werd’ ich es ertragen,


[11/0017]

Daß mich mein Mann verließ?
Wem kann mein Leid ich klagen,
O Gott, wie rührt mich dies?
Jch werde Dein gedenken
Des Morgens beim Kaffee,
Wenn ich Dir ein will ſchenken,
Die leere Taſſe ſeh’,
Kann keinen Gruß Dir winken;
Aus Jammer werd’ ich gewiß
Jhn ſchwarz und bitter trinken, ach!


Eiſenſtein.
O Gott! wie rührt mich dies!


Alle Drei.
O Gott! wie rührt mich dies!
O je, o je, wie rührt mich dies!
:,: Du, Du, Du, Du, Du, Du! :,:
O je, wie rührt mich dies!


Roſalinde.
Wo bleibt die traute Gruppe,
Kommt Mittag dann heran?
Zum Rindfleiſch — wie zur Suppe —
Zum Braten — keinen Mann!
Und ſinkt der nächt’ge Schleier,
Gibt’s wieder mir ein’ Riß,
Mein Schmerz wird ungeheuer.


Alle Drei.
O je, o je, wie rührt mich dies!
:,: Du, Du, Du, Du, Du, Du! :,:
O je, wie rührt mich dies!


Eiſenſtein.
Was ſoll das Klagen frommen!
Den Kopf verlier’ ich ſchier!


Roſalinde.
Mein Kopf iſt ganz benommen!


Adele.
Den meinen hab’ ich hier!


Eiſenſtein.
Lebt wohl, ich muß nun gehen!


[12/0018]

Roſalinde. Adele.
Leb’ wohl, Du mußt
er muß
nun gehen.


Alle Drei.
Doch bleibt ein Troſt ſo ſüß!


Roſalinde. Adele.
:,: Es gibt ein Wiederſeh’n. :,:


Alle Drei.
Es gibt ein Wiederſeh’n;
:,: O Gott, wie rührt mich dies!
O je, o je, wie rührt mich dies! :,:

Nr. 5. Finale.

Alfred.
Trinke, Liebchen, trinke ſchnell,
Trinken macht die Augen hell.
Sind die ſchönen Aeuglein klar,
Siehſt Du Alles licht und wahr.
Siehſt wie heiße Lieb’ ein Traum,
Der uns äffet ſehr,
Siehſt, wie ew’ge Treu nur Schaum —
So was gibt’s nicht mehr!
Flieht auch manche Jlluſion,
Die einſt Dein Herz erfreut,
Gibt der Wein Dir Tröſtung ſchon
Durch Vergeſſenheit!
:,: Glücklich iſt,
Wer vergißt,
Was nicht mehr zu ändern iſt. :,:
Sing’ — ſing’ — ſing’
Trink’ mit mir,
Sing’ mit mir,
Lala —
Sing’ — ſing’ — ſing’,
Trink’ mit mir,


Sing’ mit mir,
Sing’ — ſing’ — ſing’!


Roſalinde.
Was thut man hier?


[13/0019]

Beide.
:,: Glücklich iſt, wer vergißt,
Was doch nicht zu ändern iſt. :,:


Roſalinde.
Er geht nicht von hinnen,
Schläft hier wohl noch ein,
Was ſoll ich beginnen?


Alfred.
Stoß an!


Roſalinde.
Nein, nein, nein!


Alfred. Stoß an!


Roſalinde.
Nein, nein, nein! Ach!


Alfred.
Trinke, Liebchen, trinke ſchnell,
Trinken macht die Augen hell.
Mach doch nur kein böſ’ Geſicht,
Sei hübſch luſtig, grolle nicht!
Brach’ſt Du einmal auch die Treu’,
Das ſei Dir verzieh’n!
Schwöre wieder mir auf’s Neu’,
Und ich glaub’ es kühn!
Glücklich macht uns Jlluſion,
Jſt auch kurz die ganze Freud’,
Sei getroſt, ich glaub’ Dir ſchon
Und bin glücklich heut’!
:,: Glücklich iſt, wer vergißt,
Was doch nicht zu ändern iſt. :,:


Alfred.
Trinke, Liebchen, trinke ſchnell,
Trinken macht die Augen hell.


Roſalinde.
So ſchweigen Sie doch, wir ſind nicht allein.


Alfred.
Das genirt mich nicht!
Kling — kling — kling,
Sing’ — ſing’ — ſing’,
Trink’ mit mir —


[14/0020]

Sing’ mit mir!
Sing’ — ſing’ — ſing’!


Alfred.
Nein. Glücklich iſt,
Wer vergißt,
Was doch nicht zu ändern iſt.
Trink’ mit mir —
Sing’ mit mir —
Sing’ — ſing’ — ſing’. —


Frank und Alfred.
:,: Glücklich iſt,
Wer vergißt,
Was nicht mehr zu ändern iſt. :,:


Frank.
Sie ſehen, ich kann auch gemüthlich ſein,
Nun kommen Sie, mein Herr von Eiſenſtein!


Roſalinde.
Was ſoll ich thun? O welche Pein!


Alfred.
Jch bin nicht Herr von Eiſenſtein,
Bin nicht der, den Sie ſuchen!


Frank.
Sie ſind es nicht?


Alfred.
Zum Wetter, nein!


Frank.
Nur Ruhe, nicht gleich fluchen.


Roſalinde.
Sie müſſen jetzt mein Gatte ſein?


Frank.
Sollt’ ich hier hintergangen ſein?


Roſalinde.
Mein Herr, was dächten Sie von mir,
Säß’ ich mit einem Fremden hier,
Das wär’ doch ſonderbar.
Mit ſolchen Zweifeln treten da
Sie wahrlich meiner Ehr’ zu nah,
Beleid’gen mich fürwahr!


[15/0021]

Spricht denn dieſe Situation
Hier nicht klar und deutlich ſchon?
Mit mir ſo ſpät
Jm tête-à-tête
Ganz traulich und allein,
Jn dem Coſtüm,
So ganz intim,
Kann nur allein der Gatte ſein!


Frank. Alfred. Roſalinde (repet.).
Mit ihr

mir
ſo ſpät
Jm tête-à-tê u. ſ. w.


Roſalinde.
Gleich einem Paſcha fanden Sie
Jhn mir im Schlafrock vis-à-vis,
Die Mütze auf dem Haupt —
Daß man bei ſolchem Bilde doch
Ein wenig zweifeln könnte noch,
Das hätt’ ich nie geglaubt!
O ſehen Sie doch, wie er gähnt,
Wie er ſich nach Ruhe ſehnt!
Jm tête-à-tête ꝛc. ꝛc.
Mit mir ſo ſpät,
Schlief er beinah’ ſchon ein; —
So ennuyirt
Und ſo blaſirt
Kann nur allein ein Eh’mann ſein; —
Kann nur allein der Gatte ſein.


Alle Drei (repet.)
Jm tête-à-tête ꝛc. ꝛc.


Frank.
Nein, nein, ich zweifle gar nicht mehr,
Doch da ich fort nun muß,
So geben Sie — ich bitte ſehr —
Sich ſchnell den Abſchiedskuß!


Roſalinde.
Den Abſchiedskuß?


Frank und Alfred.
Den Abſchiedskuß!


[16/0022]

Roſalinde.
Nun denn — wenn es ſein muß, —
Da haben Sie den Kuß!


Alfred.
Soll ich ſchon brummen müſſen
Für Jhren werthen Herrn Gemahl,
Kann ich für ihn auch küſſen,
Fromm Weibchen, küß’ mich noch einmal!


Frank.
Mein Herr, ich bin etwas preſſirt,
Da ich heut’ ſelbſt noch invitirt.
D’rum laſſen Sie uns geh’n,
Ja, laſſen endlich Sie uns geh’n!


Roſalinde.
Sie finden gewiß
Dort meinen Gemahl.


Alfred.
Wir brummen vielleicht
Jn demſelben Local.


Roſalinde.
O ſchonen Sie mich!


Alfred.
Ganz ſicherlich!


Alfred und Roſalinde.
O ſchonen Sie mich
Ganz ſicherlich.


Frank.
Folgen Sie nur ſchnell,
Der Wagen iſt zur Stell’.
Drum fort, drum fort nur ſchnell!
Mein ſchönes großes Vogelhaus,
Es iſt ganz nahe hier!
Viel Vögel flattern ein und aus,
Bekommen frei Quartier,
Drum lad’ ich Sie ganz höflich ein,
Verehrteſter, ich bitt’
:,: Dort auch mein werther Gaſt zu ſein, :,:
Verehrteſter, ich bitt’,
Jch bitt’, ſpaziern’s gefälligſt mit.


[17/0023]

Alfred.
Wenn es ſein muß — ſo will ich geh’n.


Roſalinde.
Doch ſchweigen Sie!


Alfred.
Es ſoll geſcheh’n!


Frank.
Nur fort, ſchnell fort!


Alfred.
Gleich will ich mich bequemen,
Doch erſt noch Abſchied nehmen!


Roſalinde.
Genug, mein Herr, es iſt ſchon gut!


Alfred.
Ein Küßchen noch,
Dann hab’ ich Muth!


Roſalinde.
Nein, nein, genug,
Wir müſſen ſcheiden!


Alfred.
Ein Küßchen gibt Troſt mir im Leiden!


Frank.
Mein Herr, genug der Zärtlichkeit,
Wir kommen nicht zu Ende heut’,
Genug, es iſt ſchon Zeit!


Alle Drei.
Mein
Sein
ſchönes großes Vogelhaus,
Es iſt ganz nahe hier,
Viel Vögel flattern ein und aus
Und finden frei Quartier,
Er ladet Sie
mich
ganz höflich ein,
Drum lad’ ich Sie

Dort auch ſein
mein
Gaſt zu ſein.


2
[18/0024]

Roſalinde.
Drum bitt’ ich, fügen Sie ſich drein,
Es muß ja leider ſein.
Jch füge vorderhand mich drein,
Das wird das Beſte ſein.


Frank.
Jch bitte, fügen Sie ſich drein,
Das wird das Beſte ſein.


Roſalinde.
Ach, ja leider.
Ach, leider muß es ſein, leider muß es ſein.


Alfred.
Das wird wohl vorderhand das Allerbeſte ſein.


Frank.
Jch bitte, fügen Sie ſich drein,
Es muß ja geſchieden ſein!
Ohne Umſtänd’ nun, denn es muß ja ſein!


Roſalinde.
Nun wohlan, das Schickſal will,
Daß ich allein ſoll heut’ ſoupiren,
Ja, ich füge willig mich darein!


Alfred.
Ach, wie gern möcht’ hier mit Jhnen ich ſoupiren,
Aber wie mir ſcheinet, ſoll’s nicht ſein!


Frank.
Kommen Sie, ich ſelbſt will heute auch ſoupiren,
Fügen Sie ſich endlich doch darein!


Roſalinde.
Warum ſoll man noch vergeblich ſtreiten hier und
lamentiren?
Fort, nur fort, es muß, es muß ja ſein!


Alfred.
Ach, das Schickſal will mich von hinnen führen!
Fort, denn fort, es muß ja ſein, es muß ja ſein!


Frank. Laſſen Sie ſich ohne Umſtänd’ arretiren.
Fort, nur fort, es muß ja ſein :,: :,:


Roſalinde. Ach, es muß ja ſein!


Alfred. Es muß ja ſein!


Frank. Ja, es muß ja ſein!


[19/0025]
Zweiter Act.
Nr. 6. Chor.
Ein Souper heut’ uns winkt,
Wie noch gar keins dageweſen;
Delicat und auserleſen
Jmmer hier man ſpeiſt und trinkt,
Alles, was mit Glanz die Räume füllt,
Erſcheint uns wie ein Traumgebild,
Wie in einen Zauberkreis gebannt,
Ruft Alles: ha charmant!
Jch, charmant — amüſant!
Amüſant — amüſant!
Ein Souper uns heute winkt
ꝛc. ꝛc. ꝛc.


1. Diener: Gefror’nes?!


2. Diener: Limonade!


Fauſtina. Hier, ich bitte ſehr.


3. Diener! Confituren?!


Felicita. Hier.


4. Diener: Chocolade?


Sidi. Hier.


Mini. Mir eine Taſſe Thee!


Hermine. Jch bitte um Kaffee.


2. Diener: Sogleich.


Chor: Hier ein Thee!


Alle.
Wie fliehen ſchnell die Stunden fort,
Die Zeit wird ſicher Keinem lang,
Es heißt ja hier das Loſungswort:
Amüſement — Amüſement!

Nr. 7. Couplet.

Orlofsky.
Jch lade gern mir Gäſte ein,
Man lebt bei mir recht fein,
Man unterhält ſich wie man mag,


2*
[20/0026]

Oft bis zum hellen Tag!
Zwar langweil’ ich mich ſtets dabei,
Was man auch treibt und ſpricht;
Jndeß, was mir als Wirth ſteht frei,
Duld’ ich bei Gäſten nicht!
Und ſehe ich, es ennuyirt
Sich Jemand hier bei mir,
:,: So pack’ ich ihn ganz ungenirt,
Werf’ ihn hinaus zur Thür. :,:
Und fragen Sie — ich bitte,
:,: Warum ich das wohl thu’, :,:
:,: S’ iſt ’mal bei mir ſo Sitte
Chacun à son goût! :,:
Wenn ich mit Anderen ſitz’ beim Wein,
Und Flaſch’ um Flaſche leer’ —
Muß Jeder mit mir luſtig ſein,
Sonſt werde grob ich ſehr!
Und ſchänke Glas um Glas ich ein,
Duld’ ich nicht Widerſpruch,
Nicht leiden kann ich’s, wenn Sie ſchrei’n —
Jch will nicht — hab’ genug!
Wer mir beim Trinken nicht parirt,
Sich zieret wie ein Tropf, —
:,: Dem werfe ich ganz ungenirt
Die Flaſche an den Kopf. :,:
Und fragen Sie — ich bitte,
:,: Warum ich das wohl thu’ :,:
S’ iſt ’mal bei mir ſo Sitte
Chacun à son goût! :,:

Nr. 8. Chor und Lach-Couplet.

Orlofsky.
Ach, meine Herren und Damen,
Hier gibt’s einen Spaß!


Falke.
Zur rechten Zeit Sie kamen.


Chor.
Was gibt’s? Erzählt doch was?


[21/0027]

Orlofsky.
Seh’n Sie, dies Fräulein zierlich,
Die hält der Herr Marquis
Für — nein, es iſt poſſirlich!


Die Damen.
Für was denn?


Falke.
Rathen Sie!


Adele.
Für eine Zofe hält er mich,
Jſt das nicht lächerlich?


Alle.
Hahaha! Ja, ja, ja, ja!
Das iſt ſehr lächerlich!


Orlofsky.
Mein Herr, das iſt nicht ſehr galant,
Wie kann man ſich ſo irren?
Wie ungalant!


Falke. Wie ungalant!


Eiſenſtein.
Die Aehnlichkeit iſt ſo frappant,
Das mußte mich verwirren.


Adele.
Mein Herr Marquis,
Ein Mann wie Sie
Sollt’ beſſer das verſteh’n,
Darum rathe ich,
Ja genauer ſich
Die Leute anzuſeh’n!
Die Hand iſt doch wohl gar zu fein, ach!
Das Füßchen ſo zierlich und klein, ach!
Die Sprache, die ich führe,
Die Taille, die Tournüre,
:,: Dergleichen finden Sie
Bei einer Zofe nie :,:
Geſtehen müſſen Sie fürwahr,
Sehr komiſch dieſer Jrrthum war.
Ja ſehr komiſch, hahaha!
Jſt die Sache, hahaha!


[22/0028]

D’rum verzeih’n Sie, hahaha!
Wenn ich lache, hahaha.


Alle.
Ja ſehr komiſch, u. ſ. w. — Hahaha!
Sehr komiſch, Herr Marquis, ſind Sie!


Adele.
Mit dem Profil
Jm griech’ſchen Styl,
Beſchenkte mich Natur,
Wenn nicht das Geſicht
Schon genügend ſpricht,
Seh’n Sie die Figur!
Schau’n durch die Lorgnette Sie dann
Sich dieſe Taille nur an, ha!
Es ſcheint wohl die Liebe
Macht ihre Augen trübe,
:,: Der ſchönen Zofe Bild
Hat ganz Jhr Herz erfüllt :,:
Nun ſehen Sie ſie überall,
Sehr komiſch iſt fürwahr der Fall!
Ja ſehr komiſch, hahaha!
Jſt die Sache, hahaha! u. ſ. w.

Nr. 3. Duett.

Roſalinde. Eiſenſtein.
Eiſenſtein.
Dieſer Anſtand, ſo manierlich,
Dieſe Taille fein und zierlich,
Und ein Füßchen,
Das mit Küßchen
Glühend man bedecken ſollt!
Wenn ſie’s nur erlauben wollt!


Roſalinde.
Statt zu ſchmachten im Arreſte,
Amüſirt er ſich auf’s Beſte,
Denkt an’s Küſſen,


[23/0029]

Statt an’s Büßen,
Warte nur, Du Böſewicht,
Du entgehſt der Strafe nicht!


Eiſenſtein.
Ach, wie leicht könnt’ es entſchweben,
Dieſes holde Zauberbild,
Willſt Du nicht die Maske heben,
Die Dein Antlitz mir verhüllt?


Roſalinde.
Ei, mein ſchöner Herr, ich bitte,
Nicht verwegen, nichts berührt,
Denn es heiſcht die gute Sitte,
Daß man Masken reſpectirt!


Roſalinde.
Wie er girret — kokettiret,
Wie er ſchmachtend mich fixiret.
Keine Mahnung — keine Ahnung,
Kündet ihm, wer vor ihm ſteht!
Ja, bald werd’ ich reuſſiren,
Und den Frevler überführen,
Will’s probiren, ob er in die Falle geht?


Eiſenſtein.
Halb verwirret — halb gerühret,
Kokettiret ſie mit mir,
Laß doch ſeh’n, — ob es geht,
Ob ſie widerſteht?
Ja, bald werd’ ich reuſſiren,
Jch will doch ſeh’n — ob ſie mir widerſteht,
Ob ſie in die Falle geht!


Roſalinde.
Ach, wie wird mein Auge trübe,
Wie das Herz ſo bang mir ſchlägt!


Eiſenſtein.
Ha, ſchon meldet ſich die Liebe,
Die das Herz ihr bang bewegt!


Roſalinde.
Leider iſt’s ein altes Uebel,
Doch vorübergehend nur,


[24/0030]

Stimmen meines Herzens Schläge,
Mit dem Tiktak meiner Uhr!


Eiſenſtein.
Ei, das können wir gleich ſeh’n!


Roſalinde.
Zählen wir, ich bitte ſchön!


Beide.
Ja, zählen wir!
Eins — zwei — drei — vier! —


Roſalinde.
Fünf — 6 — 7 — 9


Eiſenſtein.
Nein, das kann nicht ſein,
Denn nach der Sieben kommt die Acht!


Roſalinde.
Sie haben mich ganz verwirrt gemacht,
Wir wollen wechſeln! —


Eiſenſtein.
Wechſeln? Wie?


Roſalinde.
Den Schlag des Herzens zählen Sie,
Und ich das Tiktak Jhrer Uhr —
Jch bitt’ auf fünf Minuten nur!
Jetzt zählen Sie,
Mein Herr Marquis!


Eiſenſtein (lebhaft).
Bin ſchon dabei —


Beide.
Eins, 2, 3, 4, 5, 6, 7, 8, 9, 10,
11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
30, 40, 50, 60, 80, 100.


Eiſenſtein.
Eins, 2, 3, 4, 5, 6, 7, 8 Hopp,
Hopp, hopp, hopp!
Das geht im Galopp!
6, 7, 8, 9, 10, 11, 12. Hopp, hopp,
Hopp, hopp!
Jm Galopp, Sechshundert und Neun!


[25/0031]

Roſalinde.
So weit können wir noch nicht ſein.


Eiſenſtein.
O ich bin weiter ſchon.


Roſalinde.
Nein — nein — nein!


Eiſenſtein.
Eine halbe Million,
Ja eine halbe Million!


Roſalinde.
Wie kann man gar ſo grob nur fehlen.


Eiſenſtein.
Da mag der Teufel richtig zählen!


Roſalinde.
Heut’ wirſt Du nicht mehr repetiren.


Eiſenſtein.
Sie will die Uhr ſich annexiren, —
Meine Uhr!


Roſalinde.
Jch danke vom Herzen! —


Eiſenſtein.
Jch wollte nur —


Roſalinde.
Belieben zu ſcherzen.


Eiſenſtein.
Sie iſt nicht in’s Netz gegangen,
Hat die Uhr mir abgefangen.
Dieſer Spaß iſt etwas theuer,
Hat blamirt mich ungeheuer.
Ach meine Uhr — ich bitte ſehr!
Jch wollte nur, —
Sie iſt nicht in’s Netz gegangen!
Ach, meine Uhr — hätte ich ſie wieder nur?
O weh — o weh!
Dieſer Spaß iſt etwas theuer,
Hat blamirt mich ungeheuer,
Meine Uhr iſt annexirt,


[26/0032]

Ach, ich bin blamirt!
Weh’ mir;

Nr. 12. Arie.

Roſalinde.
Klänge der Heimat,
Jhr weckt mir das Sehnen,
Rufet die Thräne in’s Auge mir!
Wenn ich euch höre,
Jhr himmliſchen Lieder,
Zieht mich’s wieder,
Mein Ungarland zu Dir!
O Heimat, ſo wunderbar,
Wie ſtrahlt dort die Sonne ſo klar,
Wie grün Deine Wälder,
Wie lachend die Felder,
O Land, wo ſo glücklich ich war!
Ja, Dein geliebtes Bild
Meine Seele ſo ganz erfüllt,
Und ich bin auch von Dir, von Dir weit!
Ach — weit — ach —
Dir bleibt in Ewigkeit
Doch mein Sinn immerdar
Ganz allein geweiht!
O Heimat, ſo wunderbar, u. ſ. w.


Friſchka.
Feuer, Lebensluſt, ſchwellt echte Ungarbruſt
Hei, zum Tanze ſchnell,
Cſardas tönt ſo hell.
Braves Mägdlein, mußt eine Tänz’rin ſein,
Reich’ den Arm geſchwind, dunkeläugig’ Kind!
Zur Fiedel klingen, ha — ha.
Tönet jauchzend Singen! Hahaha!
Mit dem Sporn geklirrt.
Wenn dann die Maid verwirrt
Senkt zur Erd’ den Blick.
Das verkündet Glück.


[27/0033]

Durſt’ge Zecher,
Greift zum Becher!
Laßt ihn kreiſen,
Laßt ihn kreiſen ſchnell,
Von Hand zu Hand!
Schlürft das Feuer im Tokayer,
Bringt ein Hoch aus dem Vaterland!
Hahaha!
Feuer, Lebensluſt, ſchwellt echte Ungarbruſt!
Hei, zum Tanze ſchnell,
Cſardas tönt ſo hell. — Lalala!

Nr. 13. Finale.

Orlofsky.
Jm Feuerſtrom der Reben,
Lala, lala, lala!
Sprüht ein himmliſch Leben,
Lala, lala!
Die Könige, die Kaiſer,
Sie lieben Lorbeerreiſer,
Doch lieben ſie daneben
Süßen Saft der Reben!
Stoßt an! Stoßt an!
Und huldigt im Vereine,
:,: Dem König aller Weine. :,:


Alle.
Stoßt an! Stoßt an!


Orlofsky.
Die Majeſtät wird anerkannt
Rings im Land, rings im Land!
Jubelnd wird Champagner der
Erſte ſie genannt!


Alle.
Die Majeſtät wird anerkannt, u. ſ. w.
Es lebe Champagner der Erſte!


Eiſenſtein.
Der Mönch in ſtiller Zelle
Lalala!


[28/0034]

Labt ſich an der Quelle
Lalala!
Zu netzen ſeine Lippen,
Muß viel und oft er nippen
Und holt ſich aus dem Glaſe
Rubinen auf die Naſe,
Stoßt an — ſtoßt an!
ꝛc. ꝛc.


Adele.
Dir huld’gen die Nationen
Lala —
Bis zu den fernſten Zonen
Lala —
Champagner ſchwemmt munter
Gar Mancherlei herunter,
D’rum laſſen weiſe Fürſten
Die Völker niemals dürſten!
Stoßt an! Stoßt an! ꝛc. ꝛc.


Eiſenſtein.
Herr Chevalier, ich grüße Sie!


Frank.
Merci — merci — merci! —
Auf Jhr Specielles, Herr Marquis!


Eiſenſtein.
Merci — merci — merci!


Alle.
Auf Jhr Wohl! Merci, merci!


Falke.
Halt! Hört mich an:
Was ich erſann!


Chor.
Hört ihn an!


Falke.
Jch ſehe, daß ſich die Paare gefunden,
Daß manche Herzen in Liebe verbunden,
D’rum laſſet uns Alle ein großer Verein
Von Schweſtern und von Brüdern ſein!


Orlofsky.
:,: Eine große Brüderſchaft? Es ſei! :,:


[29/0035]

Eiſenſtein.
Auch Jhr, ſchöne Maske, ſeid dabei?


Roſalinde.
Wo Alle küſſen,
Werd’ ich’s auch müſſen!


Falke.
Folgt meinem Beiſpiel, das Glas zur Hand!
Und Jeder ſingt zum Nachbar gewandt:
Brüderlein und Schweſterlein
Wollen Alle wir ſein,
Stimmet mit mir ein!
Brüderlein und Schweſterlein!
Laßt das traute Du uns ſchenken
Für die Ewigkeit —
Jmmer ſo wie heut’ —
Wenn wir morgen noch d’ran denken!
:,: Erſt ein Kuß — dann ein Du :,:
Du, Du, Du immerzu!


Alle.
Brüderlein und Schweſterlein,
Stimmet alle mit mir ein;
Laßt das traute Du uns ſchenken u. ſ. w.
Du — Du — Du — Du!
Dui — Dui — Dui Duliä ꝛc.


Orlofsky.
Genug damit, genug! Dieſe Tänzer mögen ruh’n!
Bei rauſchender Weiſe, im fröhlichen Kreiſe
Laſſet uns ſelbſt hintanzen nun!
Stellt Euch zum Tanz!


Chor.
Ja, ja, ein Tanz, ein wirbelnder Tanz
Erhöht des Feſtes Glanz!
Ha, welch’ ein Feſt,
Welche Nacht voll Freud’!
Liebe und Wein gibt uns Seligkeit!
Ging’s durch das Leben ſo flott wie heut’,
Wär’ jede Stunde der Luſt geweiht!
Du biſt meine Stütze, Freund!


[30/0036]

Frank.
Ja, Deine Stütze für’s Leben!


Roſalinde, Falke, Orlofsky.
Welch’ ein rührend Wiederſehen
Wird das im Arreſte geben.


Frank.
Brüderl, meine Uhr geht ſchlecht;
Schau, wie viel’s auf Deiner iſt?


Eiſenſtein.
Brüderl, meine Uhr geht auch nicht recht
Weil ſie ſchon gegangen iſt.
Holde, hier vor Allen,
Laß’ die Maske endlich fallen,
Daß ich ſehe, wer mich beſiegt,
Und wer meine Uhr gekriegt!


Roſalinde.
Verlange nicht zu ſchauen, was hier verhüllt,
Erbeben würdeſt Du vor dieſem Bild.


Eiſenſtein.
Huhuhuhu — was heißt denn das!?
Hahaha — ein guter Spaß,
Fürwahr, ein prächtiger Spaß!


Adele.
Biſt Du ein Mann, ſchau Sie Dir an!


Jda.
Zurück jetzt zu weichen wäre Blamage!


Eiſenſtein.
O ich habe ſchon Courage!
Schätzchen, ſträub’ Dich länger nicht.


Roſalinde.
Hab ’ne Warze auf der Naſe,
D’rum verberg’ ich mein Geſicht.


Eiſenſtein.
An die Warze glaub’ ich nicht.


Adele. Jda.
Nein, die Warze ſchreckt ihn nicht!


Eiſenſtein.
Sehen muß ich dies Geſicht!


[31/0037]

Frank und Eiſenſtein.
Eins — zwei — drei — vier — fünf — ſechs
:,: Meinen Hut, — meinen Hut,
S’iſt die höchſte Zeit! :,:


Alle.
Seinen Hut, ſeinen Hut!
Hört doch, wie er ſchreit!


Eiſenſtein.
Der Arreſt harret mein.


Frank.
Längſt ſollt’ ich zu Hauſe ſein!


Beide.
Meinen Hut — meinen Hut
Gebt mir meinen Hut.


Alle.
Seinen Hut — ſeinen Hut — gebt ihm ſeinen Hut.


Frank.
Eine kurze Sirecke gehſt Du mit mir.


Eiſenſtein.
An der nächſten Ecke, da ſcheiden wir!


Eiſenſtein und Frank.
So laß uns geh’n!


Jda und Falke.
Auf Wiederſeh’n! Hahahaha!


Chor.
Ha, welch’ ein Feſt, welche Nacht voll Freud’ u. ſ. w.
:,: Dann bleibt jede Stunde der Luſt geweiht. :,:


Alle.
Stoßt an und laſſet leben
Den König aller Reben,
Die Majeſtät wird anerkannt,
Sie wird Champagner der Erſte genannt.



[32/0038]
Dritter Act.


Nr. 14. Lied.

Adele.
Spiel ich die Unſchuld vom Lande
Natürlich im kurzen Gewande,
So hüpf’ ich ganz neckiſch umher,
Als ob ich ein Eichkätzchen wär’!
Und kommt ein ſauberer junger Mann.
So blinzle ich ihn lächelnd an,
Durch die Finger zwar nur,
Als ein Kind der Natur,
Und zupf’ an meinem Schürzenband,
So fängt man Spatzen auf dem Land!
Und folgt er mir, wohin ich geh’,
Sag’ ich naiv: Sö Schlimmer, Sö!
Setz’ mich zu ihm in’s Gras ſodann,
Und fang’ zuletzt zu ſingen an.
Doi — Doi — Doi! —
Wenn Sie das geſeh’n,
Müſſen Sie geſteh’n:
Es wär’ der Schade nicht gering,
Wenn mit dem Talent, mit dem Talent
Jch nicht zum Theater ging!
Spiel’ ich eine Königin,
Schreit’ ich majeſtätiſch hin,
Nicke hier — und nicke da,
Ja, ganz im meiner Gloria!
Alles macht voll Ehrfurcht mir Spalier,
Lauſcht den Tönen meines Sangs,
Lächelnd ich das Reich und Volk regier’,
Königin par excellence!
Lalala —
Spiel’ ich ’ne Dame von Paris,
Ach, die Gattin eines Herrn Marquis,
Da kommt ein junger Graf in’s Haus, ach!
Der geht auf meine Tugend aus, ach!
Zwei Act hindurch geb’ ich nicht nach,
Doch ach, im Dritten werd’ ich ſchwach!


[33/0039]

Da öffnet plötzlich ſich die Thür,
O weh, mein Mann, was wird aus mir?
Verzeihung, flöt’ ich — er verzeiht,
Ach!
Zum Schlußtableau — da weinen d’Leut’ —
Ach!

Nr. 15. Terzett.

Roſalinde. Alfred. Eiſenſtein.
Roſalinde.
Jch ſtehe voll Zagen.


Alfred.
Um Rath ihn zu fragen.


Eiſenſtein.
Pack’ ich ihn beim Kragen.


Roſalinde.
Was wird er mich fragen?


Alfred.
Muß Alles ich ſagen?


Eiſenſtein.
So würd’ er nichts ſagen!


Roſalinde.
Darf ich es wohl wagen?
Jhnen Alles zu ſagen?


Eiſenſtein.
Möcht’ nieder ihn ſchlagen.


Alfred.
Warum denn verzagen?


Alfred.
Wir werden ihn klagen.


Eiſenſtein.
Doch darf ich’s nicht wagen.


Roſalinde.
:,: Die Situation erheiſcht Discretion :,:


Alfred.
:,: Die Situation, er hilft uns denn ſchon :,:


Eiſenſtein.
:,: Darf nicht einmal droh’n,
Dem frechen Patron. :,:


3
[34/0040]

Eiſenſtein.
Jetzt bitte ich die ganze Sache
Mir haarklein zu erzählen,
Jndeß ich mir Notizen mache.


Roſalinde.
Der Fall iſt eigenthümlich,
Wie Sie gleich werden ſeh’n.


Alfred.
Sogar verwickelt ziemlich,
Das muß man eingeſteh’n.


Eiſenſtein.
Nun denn, ſo geben Sie zu Protokoll,
Worin ich Sie vertheid’gen ſoll!


Alfred.
Ein ſeltſam Abenteuer
Jſt geſtern mir paſſirt;
Man hat mich aus Verſehen
Hier in’ Arreſt geführt,
Weil ich mit dieſer Dame
Ein wenig ſpät ſoupirt.


Eiſenſtein.
Ein Glück, daß es ſo kam,
Sie handelten infam!


Alfred.
Was kommt denn Jhnen in den Sinn?
Sie ſoll’n mich ja vertheid’gen!


Eiſenſtein.
Verzeih’n Sie, wenn ich heftig bin,
Der Gegenſtand reißt mich hin;
Jch wollt Sie nicht beleid’gen,
Nein, ich ſoll Sie ja vertheid’gen!


Roſalinde. Alfred.
Mein Herr Notar,
Das war fürwahr
Sehr ſonderbar!
Nur ruhig Blut,
Denn ſolche Wuth
:,: Macht ſich für Sie
nicht gut! :,:


Eiſenſtein.
Was ich erfahr’
Verwirrt fürwahr
Mich ganz und gar,
D’rum ruhig Blut,
Jch muß die Wuth
:,: Verbergen jetzt noch
gut! :,:



[35/0041]

Roſalinde.
Das Ganze war ein Zufall,
Nichts Uebles iſt paſſirt,
Doch würd’ bekannt es werden,
Wär’ ich compromittirt!
Da ſicher mich mein Gatte
Für ſchuldig halten wird.


Eiſenſtein.
Da hätt’ er auch ganz recht,
Sie handelten ſehr ſchlecht.


Roſalinde.
Was kommt denn Jhnen in den Sinn?
Sie ſoll’n mich ja vertheidigen!


Eiſenſtein.
Verzeih’n Sie, wenn ich heftig bin ꝛc.


Roſalinde. Alfred.
Mein Herr Notar ꝛc.
Eiſenſtein.
Was ich erfahr’ ꝛc.
Jch bitt’ mir Alles zu geſteh’n
Und nichts zu übergeh’n;
Jſt kein Detail mehr überſeh’n?


Alfred.
Was ſollen dieſe Fragen hier?


Roſalinde.
Mein Herr —


Eiſenſtein.
:,: Jſt weiter nichts geſcheh’n? :,:


Roſalinde.
Mein Herr, was denken Sie von mir?
Was ſollen dieſe Fragen hier?


Eiſenſtein.
Jch frag’ Sie auf’s G’wiſſen,
Jſt weiter nichts geſcheh’n?
Denn Alles muß ich wiſſen,
Alles muß ich wiſſen,


Roſalinde. Mein Herr —
Alfred. Mein Herr —
Roſalinde.
Mein Herr — es ſcheint faſt, als empfinden Sie


3*
[36/0042]

Für meinen Gatten Sympathie,
D’rum muß ich Jhnen ſagen:
Ein Ungeheuer iſt mein Mann.
Und niemals ich vergeben kann
Sein treulos ſchändliches Betragen;
Er hat die ganze vor’ge Nacht
Mit jungen Damen zugebracht,
Lebt herrlich und in Freuden,
Doch ſchenk’ ich’s nicht dem Böſewicht,
Und kommt er wieder mir nach Haus,
Kratz’ ich ihm erſt die Augen aus,
:,: Und dann — laß’ ich mich ſcheiden. :,:


Alfred und Eiſenſtein.
So kratzt ſieihm
mir
die Augen aus
Und dann läßt ſie ſich ſcheiden!


Alfred.
Da Sie Alles wiſſen nun,
Sagen Sie, was ſoll man thun?
Geben Sie uns Mittel an,
Wie man dieſem Ehemann
Eine Naſe drehen kann?!


Eiſenſtein. Das iſt zu viel!


Alfred.
Was ſoll das ſein?


Eiſenſtein.
Welch’ ſchändlich Spiel!


Roſalinde.
Was ſoll das ſein?


Alfred und Roſalinde.
Mein Herr, wozu dies Schrei’n?


Eiſenſtein.
Erzittert, Jhr Verbrecher,
Die Strafe bricht herein,
Hier ſtehe ich als Rächer!
Jch ſelbſt bin Eiſenſtein!


Roſalinde.
Er ſelbſt iſt Eiſenſtein?


Alfred.
Er ſelbſt iſt Eiſenſtein?


[37/0043]

Eiſenſtein.
:,: Ja, ich bin’s, den Jhr betrogen!
Ja, ich bin’s, den Jhr belogen!
Aber rächen will ich mich
Jetzt fürchterlich. :,:


Roſalinde.
Hat er ſelbſt mich doch betrogen,
Treulos angeführt, belogen, —
Und nun will er rächen ſich?
Die Beleidigte bin ich!
Kein Verzeih’n!


Alfred. Der Eiſenſtein.


Roſalinde. Kein Bereu’n!


Alfred. Der Eiſenſtein.


Roſalinde. Jch allein —


Alle Drei.
Will Rache ſchrei’n, Rache!


Roſalinde. Kein Verzeih’n!


Alfred und Eiſenſtein.
Der Eiſenſtein.


Roſalinde. Kein Bereu’n!


Alfred und Eiſenſtein.
Der Eiſenſtein will Rache,
Fürchterlich nach Rache ſchrei’n!


Roſalinde.
So hören Sie mich endlich an!


Alfred.
So nehmen Sie Vernunft doch an!


Eiſenſtein.
Sie wagen noch zu reden, Mann!
Und haben meinen Schlafrock an?


Alfred.
Dies iſt Jhr Schlafrock, ich geſteh’!


Roſalinde.
Verhängnißvoller Schlafrock; Weh’!


Eiſenſtein.
Ha! Dies Jndicium
Macht Sie beide ſtumm!


Roſalinde.
Hat er ſelbſt mich doch betrogen ꝛc.


[38/0044]
Nr. 16. Finale.

Alle.
O Fledermaus! O Fledermaus!
Laß endlich jetzt dein Opfer aus!
:,: Der arme Mann :,:
Jſt gar zu übel d’ran!


Eiſenſtein.
Woll’n Sie mir erklären nicht,
Was ſoll bedeuten die Geſchicht’?
Noch werde ich nicht klug daraus!


Falke.
So rächt ſich die Fledermaus!


Alle.
So rächt ſich die Fledermaus!
Doch — —
O Fledermaus — o Fledermaus,
Laß endlich jetzt dein Opfer aus; u. ſ. w.


Eiſenſtein.
So erklärt mir doch, ich bitt’!


Falke.
Alles, was Dir Sorge macht,
War ein Scherz, von mir erdacht.


Alle.
Und wir Alle ſpielten mit!


Eiſenſtein.
Wie? Der Prinz?


Orlofsky.
Jch ſpielte mit!


Eiſenſtein.
Und Adele?


Adele. Jch ſpielte mit!


Eiſenſtein. Jhr Souper?


Alfred.
War nichts als Mythe!


Eiſenſtein.
Doch mein Schlafrock?


Roſalinde. Requiſite!


[39/0045]

Eiſenſtein.
Wonne, Seligkeit, Entzücken!
O, wie macht dies Wort mich froh.
Gattin, laß an’s Herz Dich drücken! —


Alfred.
War auch nicht g’rad’ Alles ſo,
Wir wollen ihm den Glauben,
Der ihn beglückt, nicht rauben.


Adele.
Nun, was geſchieht mit mir?


Frank.
Bleiben im Arreſt Sie hier,
Will ich Sie als Freund und Vater
Bilden laſſen für’s Theater.


Orlofsky.
Nein, ich laß als Kunſtmäcen
Solch’ Talent mir nicht entgeh’n;
Das iſt bei mir ſo Sitte —
Chacun à son goût!


Roſalinde.
Champagner hat’s verſchuldet,
Lalala —
Was wir heut’ erduldet,
Lalala —
Doch gab er mir auch Wahrheit,
Und zeigt in vollſter Klarheit
Mir meines Gatten Treue
Und führte ihn zur Reue!
Stimmt ein, ſtimmt ein
Und huldigt im Vereine
:,: Dem König aller Weine. :,:


Alle.
Stoßt an! Stimmt ein! —


Roſalinde.
Die Majeſtät wird anerkannt,
Anerkannt, — rings im Land!
Jubelnd wird Champagner
Der Erſte ſie genannt!


Alle (rep.).
Die Majeſtät ꝛc.
Ende.


[40/0046]
Die kaiſ. kön. Hof-
Mulikalien-Handlung
von
Guſtan Lewy
in
Wien
I. Bezirk, Petersplatz Nr. 15

empfiehlt ihr beſtaſſortirtes Muſikalien-Lager, ſowie Leih-
Jnſtitut den P. T. Muſikfreunden. Dem Leih-Jnſtitute
ſind ſämmtliche im Drucke erſchienenen Clavier-Auszüge
von Opern, Operetten ꝛc. mit und ohne Text einverleibt.

Das Muſikalien-Lager enthält außer ſämmtlich erſchie-
nenen Editionen vom Verlag der Carl Millöcker-
ſchen Operetten:

Apajune, der Waſſermann
und
Die Jungfrau von Belleville
in allen Arrangements.


Der Arienbücher-Verlag enthält:


Apajune der Waſſermann
Carneval in Rom, der.
Ceſarine.
Dubarry Gräfin.
Fledermans, die.

Jnngfrau von Belleville, die.
Porträtdame, die.
Prinz Methnſalem.
Seecadet, der.
Spitzeutuch der Königin, das.



[0047]

[0048]

[0049]

